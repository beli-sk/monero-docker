FROM alpine
MAINTAINER Michal Belica <code@beli.sk>

RUN apk add --no-cache gnupg \
	&& wget -O - https://raw.githubusercontent.com/monero-project/monero/master/utils/gpg_keys/binaryfate.asc \
		| gpg --import \
	&& gpg --list-keys 81AC591FE9C4B65C5806AFC3F0AF4D462A0BDF92 \
	&& wget -O monero.tar.bz2 https://downloads.getmonero.org/cli/linux64 \
	&& wget https://getmonero.org/downloads/hashes.txt \
	&& gpg --verify hashes.txt \
	&& echo `grep monero-linux-x64 hashes.txt | cut -d' ' -f1`'  monero.tar.bz2' > hash \
	&& sha256sum -c hash
RUN tar -jxvf monero.tar.bz2

FROM debian:stable-slim
COPY --from=0 /monero-x86_64-linux-gnu*/* /usr/local/bin/
COPY bitmonero.conf /
RUN useradd -d /home/user -m user \
	&& mkdir /bitmonero \
	&& chown user:user /bitmonero.conf /bitmonero
USER user
ENV HOME /home/user
CMD ["/usr/local/bin/monerod", "--config-file=/bitmonero.conf", "--non-interactive"]
